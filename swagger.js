const swaggerAutogen = require('swagger-autogen')()

const outputFile = './swagger_output.json';
const endpointsFiles = ['./api/postes.js','./api/candidats.js','./api/candidatures.js','./api/postes_soumis.js'];

swaggerAutogen(outputFile, endpointsFiles).then(() => {
    require('./app.js')
})
const createError = require('http-errors');
const express = require('express');
var path = require('path');
const cors = require('cors');
const bodyParser = require('body-parser');
var logger = require('morgan');
const mongoose = require('mongoose');
const passport = require('passport');
var postesRouter = require('./api/postes');
var candidatsRouter = require('./api/candidats');
var postesSoumisRouter = require('./api/postes_soumis');
var candidaturesRouter = require('./api/candidatures');
const swaggerUi = require('swagger-ui-express')
const swaggerFile = require('./swagger_output.json');
require('dotenv').config();
const app = express()

//for swagger interface generated
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerFile))

// app.use(cors(corsOptions));
app.use(cors());

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
// parse requests of content-type - application/json
app.use(bodyParser.json());

// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }));


// //CORS bypass
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization, x-access-token");
  res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
  next();
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use('/static', express.static(path.join(__dirname, 'public')));


app.use(express.json());


console.log(`${process.env.DB_CONNEXION_STRING}`);
mongoose.connect(`${process.env.DB_CONNEXION_STRING}`,{useNewUrlParser:true, useUnifiedTopology:true, useCreateIndex: true});
const connection=mongoose.connection;
connection.once('open',()=>{
    console.log("Mongodb database connection successfully");
});




// import routing

app.use('/api', postesRouter);
app.use('/api', candidatsRouter);
app.use('/api', postesSoumisRouter);
app.use('/api', candidaturesRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});


module.exports = app;

//modules importation
const PostesSoumis  = require('../models/postes_soumis');

//create new poste_soumis
exports.create = (req, res, next)=>{

//verify if poste_id and date_expiration exist
if (req.body.date_expiration != undefined && req.body.date_expiration != '') {
   
   let poste_soumis = new PostesSoumis({
       poste_id: req.body.poste_id,
       date_expiration: req.body.date_expiration,
    });

   //check if data exist before saved
   PostesSoumis.find({ poste_id: req.body.poste_id, date_expiration: req.body.date_expiration}).count()
        .then(length => {
          if (length > 0 ) { res.json('theses data already exist'); }
          else{
    		    poste_soumis.save()
       		   .then(poste_soumis => {
               res.json(poste_soumis)
            })
        	.catch(err => {
            res.status(500).json(err.message)
          });
   		 }

      }).catch(err => res.status(500).json(err.message));
 }else{
    res.json('try to enter correct value for date_expiration field');
 }
};

//take all data
exports.find_all = (req, res, next) => {
    PostesSoumis.find({},{createdAt: 1, date_expiration: 1})
        .populate('poste_id','libelle description')
        .sort({createdAt: -1})
        .then(poste_soumis => res.json(poste_soumis))
        .catch(err => res.status(500).json(err));
}

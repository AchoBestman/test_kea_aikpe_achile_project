//modules importation
const Postes  = require('../models/postes');

//store a new post
exports.create = (req, res, next)=>{

    //Poste model initialize
   let poste = new Postes({
       libelle: req.body.libelle,
       description: req.body.description,
       nbre_anne_experience: req.body.nbre_anne_experience,
       diplome: req.body.diplome,
       place_dispo: req.body.place_dispo
    });


   //check if this poste existe before saved
   if (req.body.libelle != undefined && req.body.libelle != '') {

     Postes.find({ libelle: req.body.libelle}).count()
        .then(poste_length => {
          if (poste_length > 0) { res.json('this poste already exist'); }
          else{
            poste.save()
              .then(poste => {
               res.json(poste);
            })
          .catch(err => res.status(500).json(err));
          }

         }).catch(err => res.status(500).json(err));
    }else{
      res.json('libelle does not exist');
    }

}; 

//take all poste from poste table
exports.find_all = (req, res, next) => {
    Postes.find({})
        .sort({createdAt: -1})
        .then(postes => res.json(postes))
        .catch(err => res.status(500).json(err));
}


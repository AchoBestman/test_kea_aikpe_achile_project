//modules importation
const Candidats  = require('../models/candidats');
const Candidature  = require('../models/candidatures');
const config = require('../config/auth.config');
const Token = require('../models/token');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');

//create a new data
exports.create = (req, res, next)=>{

   let candidat = new Candidats({
       nom: req.body.nom,
       prenom: req.body.prenom,
       telephone: req.body.telephone,
       email: req.body.email,
       password: req.body.password,
       proffession: req.body.proffession
    });

   // email verification
    if(validateEmail(req.body.email) && req.body.email != undefined){
      // store if data does not exist
      Candidats.find({telephone: req.body.telephone}).count()
        .then(length => {
          if (length > 0) { res.json('This phone number already used'); }
          else{
            //password encrypting
            bcrypt.genSalt(10, (err, salt) => {
              if(err) throw err;
              bcrypt.hash(candidat.password, salt, (err, hash) => {
                  if(err) throw err;
                  candidat.password = hash;
                  // data stored
                  candidat.save()
                  .then(candidat => {
                   res.json(candidat)
                    })
                  .catch(err => res.status(500).json(err.message));

              });
          });

            
        }

      }).catch(err => res.status(500).json(err));

    } else {
     res.status(500).json("Address email invalide or not exist");
    }

};

//take all data from the candidats table
exports.find_all = (req, res, next) => {
    Candidats.find({})
        .sort({createdAt: -1})
        .then(candidats => res.json(candidats))
        .catch(err => res.status(500).json(err));
}

//delete a candidat 
exports.delete = (req, res, next) => {
    Candidature.deleteOne({candidat_id: req.params.id}).then(candidats => {
      Candidats.deleteOne({_id: req.params.id}).then(candidats => {
        res.json('Candidat deleted');
    }).catch(err => res.status(500).json(err.message));
    }).catch(err => res.status(500).json(err.message));
}


//email verification function
function validateEmail(email){
    var emailReg = new RegExp(/^([\w-\.]+)@((?:[\w]+\.)+)([a-zA-Z]{2,4})/i);
    var valid = emailReg.test(email);

    if(!valid) {
        return false;
    } else {
        return true;
    }
}

//signin a candidat with his phone number and his password
exports.signin = (req, res,next) => {
    let data = [];
    Candidats.find({telephone: req.body.telephone})
        .then(candidat => {
            if(!candidat || candidat === null || !candidat.length){
                res.status(404).json({message: "Phone number not found!"});
            }else{
                candidat.forEach(candidat => {
                    isMatch = bcrypt.compareSync(req.body.password, candidat.password);
                    if(isMatch){
                        data.push(candidat);
                    }
                });
                if(data.length === 1){
                        let token = jwt.sign({ id: data[0]._id }, config.secret, {
                            expiresIn: 86400
                        });
                        
                        res.json({
                            accessToken: token,
                            candidat: data[0]
                        });
                }else if(data.length > 1){
                    res.json({
                        accessToken: null,
                        candidat: data
                    });
                }else{
                    res.status(401).json({
                        accessToken: null,
                        message: "Invalid Password !"
                    });
                }
            }
        }
    );
};
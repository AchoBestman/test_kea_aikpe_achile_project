//modules importation
const Candidature  = require('../models/candidatures');

//create a new data
exports.create = (req, res, next)=>{

   let candidature = new Candidature({
       candidat_id: req.body.candidat_id,
       poste_id: req.body.poste_id
    });

   //verify if data contain a file witch is not image, audio or video
   if(req.files){
      if(!req.files.mimetype.match('image/*') && !req.files.mimetype.match('audio/*') && !req.files.mimetype.match('video/*')){
                candidature.cv = 'http://' + req.headers.host + '/static/cv/' + req.files.filename;
      }
    
    }

    //data storage if not exist
    Candidature.find({ poste_id: req.body.poste_id, candidat_id: req.body.candidat_id}).count()
        .then(length => {
          if (length > 0 && req.body.poste_id != undefined) { res.json('data exist'); }
          else{
          candidature.save()
            .then(candidature => {
                   res.json(candidature)
                })
            .catch(err => res.status(500).json(err.message));
          }

    }).catch(err => res.status(500).json(err.message));
   
}

//take all data from the candidature table
exports.find_all = (req, res, next) => {
    Candidature.find({},{cv: 1, _id: 0})
        .populate('candidat_id','nom prenom telephone')
        .sort({createdAt: -1})
        .then(candidatures => res.json(candidatures))
        .catch(err => res.status(500).json(err.message));
}


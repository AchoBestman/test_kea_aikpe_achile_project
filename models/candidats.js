 // models/candidats.js
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {timestamps: true };

const CandidatSchema = new Schema({
	    nom: {
	        type: String,
	        default: ''
	    },
	    prenom: {
	        type: String,
	        default: ''
    	},
    	telephone: 
    	 {
	        type: Number,
	        default: 0
	     },
	    email: 
	     {
	        type: String,
	        default: ''
    	 },
    	 password: 
	     {
	        type: String,
	        default: ''
    	 },
	    proffession: {
	        type: String,
	        default: ''
	    }
	    

}, options);

module.exports = Candidats = mongoose.model('Candidats', CandidatSchema);  
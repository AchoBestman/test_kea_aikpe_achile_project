 // models/postes.js
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {timestamps: true };

const PosteSoumisSchema = new Schema({
	    poste_id: {
	        type: mongoose.Schema.Types.ObjectId,
        	ref: 'Postes'
	    },
	    date_expiration: {
	        type: Date,
	        default: ''
	    }
	    

}, options);

module.exports = PostesSoumis = mongoose.model('PostesSoumis', PosteSoumisSchema);  
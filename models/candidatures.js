 // models/canditature.js
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {timestamps: true };

const CandidatureSchema = new Schema({
	    candidat_id: {
	        type: mongoose.Schema.Types.ObjectId,
        	ref: 'Candidats'
	    },
	    poste_id: {
	        type: mongoose.Schema.Types.ObjectId,
            ref: 'Postes'
    	},
	    cv: {
	        type: String,
	        default: ''
	    }
	    

}, options);

module.exports = Candidature = mongoose.model('Candidature', CandidatureSchema);  
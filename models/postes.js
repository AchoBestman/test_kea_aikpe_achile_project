 // models/postes.js
const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const options = {timestamps: true };

const PosteSchema = new Schema({
	    libelle: {
	        type: String,
	        default: ''
	    },
	    description: {
	        type: String,
	        default: ''
    	},
    	nbre_anne_experience: [
    	 {
	        type: Number,
	        default: 0
	     }
	    ],
	    diplome: [
	     {
	        type: String,
	        default: ''
    	 }
    	],
	    place_dispo: {
	        type: String,
	        default: ''
	    }
	    

}, options);

module.exports = Postes = mongoose.model('Postes', PosteSchema);  
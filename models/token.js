const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TokenSchema = new Schema({
    candidat_id: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Candidats',
        required: true
    },
    token: {
        type: String,
        required: true
    },
    createdAt: {
        type: Date,
        required: true,
        default: Date.now,
        expires: 86400
    }
});

module.exports = Token = mongoose.model('Token', TokenSchema);

//modules importation
const multer =require('multer');
const path = require('path');
const fs = require('fs');

//using multer for files uploaded
const uploadStorage = multer.diskStorage({
    destination: (req, file, callBack) => {
        let dest = path.resolve('./public/cv');
        fs.mkdirSync(dest, { recursive: true });
        callBack(null, dest);
    },
    filename: (req, file, callBack)=>{
        callBack(null, file.originalname);
    }
})

const upload = multer({storage: uploadStorage});

//export upload variable
module.exports = upload;

let mongoose = require("mongoose");
let Candidatures = require('../models/candidatures');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let app = require('../app');
let should = chai.should();


chai.use(chaiHttp);
//Our parent block
describe('Candidatures', () => {
    beforeEach((done) => { //Before each test we empty the database
        Candidature.remove({}, (err) => {
           done();
        });
    });
/*
  * Test the /GET route
  */
  describe('/GET candidatures', () => {
      it('it should GET all the candidatures', (done) => {
        chai.request(app)
            .get('/candidatures/find_all')
            .end((err, res) => {
                  res.should.have.status(200);
                  res.body.should.be.a('array');
                  res.body.length.should.be.eql(0);
              done();
            });
      });
  });





});


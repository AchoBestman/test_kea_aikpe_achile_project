let mongoose = require("mongoose");
let Postes = require('../models/postes');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let app = require('../app');
let should = chai.should();


chai.use(chaiHttp);
//Our parent block
describe('Postes', () => {
    beforeEach((done) => { //Before each test we empty the database
        Postes.remove({}, (err) => {
           done();
        });
    });
/*
  * Test the /GET route
  */
  describe('/GET postes', () => {
      it('it should GET all the postes', (done) => {
        chai.request(app)
            .get('/postes/find_all')
            .end((err, res) => {
                  res.should.have.status(200);
                  res.body.should.be.a('array');
                  res.body.length.should.be.eql(0);
              done();
            });
      });
  });




 /*
  * Test the /POST route
  */
  describe('/POST postes', () => {
      it('it should POST a poste', (done) => {
          let poste = {
            libelle: 'libelle',
      			description: 'description',
      			nbre_anne_experience: 2,
      			diplome: 'master1',
      			place_dispo: 12
          }
        chai.request(app)
            .post('/postes/create')
            .send(poste)
            .end((err, res) => {
                  res.should.have.status(200);
              done();
            });
      });

  });



});


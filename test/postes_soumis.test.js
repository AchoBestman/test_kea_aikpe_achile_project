let mongoose = require("mongoose");
let PostesSoumis = require('../models/postes_soumis');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let app = require('../app');
let should = chai.should();


chai.use(chaiHttp);
//Our parent block
describe('postes_soumis', () => {
    beforeEach((done) => { //Before each test we empty the database
        PostesSoumis.remove({}, (err) => {
           done();
        });
    });
/*
  * Test the /GET route
  */
  describe('/GET postes_soumis', () => {
      it('it should GET all the postes_soumis', (done) => {
        chai.request(app)
            .get('/postes_soumis/find_all')
            .end((err, res) => {
                  res.should.have.status(200);
                  res.body.should.be.a('array');
                  res.body.length.should.be.eql(0);
              done();
            });
      });
  });



 /*
  * Test the /POST route
  */
  describe('/POST postes_soumis', () => {
      it('it should POST a postes_soumis', (done) => {
          let postes_soumis = {
      			poste_id: '',
      			date_expiration: ''
          }
        chai.request(app)
            .post('/postes_soumis/create')
            .send(postes_soumis)
            .end((err, res) => {
                  res.should.have.status(200);
              done();
            });
      });

  });



});


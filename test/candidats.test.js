let mongoose = require("mongoose");
let Candidats = require('../models/candidats');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let app = require('../app');
let should = chai.should();


chai.use(chaiHttp);
//Our parent block
describe('Candidats', () => {
    beforeEach((done) => { //Before each test we empty the database
        Candidats.remove({}, (err) => {
           done();
        });
    });
/*
  * Test the /GET route
  */
  describe('/GET Candidats', () => {
      it('it should GET all the Candidats', (done) => {
        chai.request(app)
            .get('/candidats/find_all')
            .end((err, res) => {
                  res.should.have.status(200);
                  res.body.should.be.a('array');
                  res.body.length.should.be.eql(0);
              done();
            });
      });
  });


 /*
  * Test the /POST route
  */
  describe('/POST Candidats', () => {
      it('it should POST a Candidats', (done) => {
          let candidat = {
            nom: 'nom',
      			prenom: 'prenom',
      			telephone: '',
      			email: 'master@gmail.com',
      			password: 'kea',
            proffession: 'informaticien'
          }
        chai.request(app)
            .post('/candidats/create')
            .send(candidat)
            .end((err, res) => {
                  res.should.have.status(200);
              done();
            });
      });

  });



});


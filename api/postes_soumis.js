//modules importation
const router =require('express').Router();
const postes_soumis=require('../controllers/postes_soumis');

//candidats routing
router.post('/postes_soumis/create',postes_soumis.create);
router.get('/postes_soumis/find_all', postes_soumis.find_all);

//module exportation
module.exports=router;  

//modules importation
const router =require('express').Router();
const candidat=require('../controllers/candidats');

//candidats routing
router.post('/candidats/create',candidat.create);
router.get('/candidats/find_all', candidat.find_all);
router.post('/candidats/signin', candidat.signin);
router.get('/candidats/delete/:id',candidat.delete)

//module exportation
module.exports=router;  

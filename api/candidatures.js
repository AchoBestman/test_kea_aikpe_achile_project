//modules importation
const router =require('express').Router();
const candidature=require('../controllers/candidatures');
const upload = require('../middlewares/multer');

//candidats routing
router.post('/candidatures/create',upload.single('file'),candidature.create);
router.get('/candidatures/find_all', candidature.find_all);

//module exportation
module.exports=router;  

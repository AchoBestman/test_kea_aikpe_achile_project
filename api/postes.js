//modules importation
const router =require('express').Router();
const poste = require('../controllers/postes');

//candidats routing
router.post('/postes/create',poste.create);
router.get('/postes/find_all', poste.find_all);

//module exportation
module.exports=router;  
